from django.contrib import admin
import csv
from django.http import HttpResponse

# Register your models here.
from .models import Usage


class HavenAdmin(admin.ModelAdmin):
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        # writer.writerow(field_names)
        for obj in self.model.objects.all():
            row = writer.writerow([getattr(obj, "log")])

        return response

    export_as_csv.short_description = "Export All"


admin.site.register(Usage, HavenAdmin)
