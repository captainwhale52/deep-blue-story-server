from django.urls import path
from django.conf.urls import url, include

from . import views

urlpatterns = [
    path('usages', views.UsageListAPIView.as_view(), name='usage-list'),
    path('usages/add', views.UsageCreateAPIView.as_view(), name='usage-add'),
]
