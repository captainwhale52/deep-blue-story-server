import uuid

import pytz
from django.db import models
# from django.contrib.auth import get_user_model
from django_resized import ResizedImageField
from django.core.validators import FileExtensionValidator
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.core.validators import validate_slug
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _, ngettext_lazy


class Usage(models.Model):
    log = models.CharField(max_length=256, null=False, blank=False, default="")

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["created_at"]

    def __str__(self):
        return f'{self.log}'
