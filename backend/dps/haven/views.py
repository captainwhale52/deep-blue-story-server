from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.db.models import Q

import time
from django.utils.decorators import method_decorator
from rest_framework.throttling import UserRateThrottle, AnonRateThrottle
from datetime import datetime, timedelta

from rest_framework.generics import (
    ListAPIView, RetrieveAPIView, DestroyAPIView, UpdateAPIView, CreateAPIView, RetrieveUpdateAPIView
)

from .models import (
    Usage
)

from .serializers import (
    UsageSerializer
)


class UsageListAPIView(ListAPIView):
    """
    Return all background assets.
    ---
    """
    permission_classes = (AllowAny,)
    queryset = Usage.objects.all()
    serializer_class = UsageSerializer



class UsageCreateAPIView(CreateAPIView):
    """
        Create a background asset for an event.
    ---
    """
    permission_classes = (AllowAny,)
    queryset = Usage.objects.all()
    serializer_class = UsageSerializer