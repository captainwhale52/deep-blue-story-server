# from django.http import HttpResponse
import os
import json
from random import randint

import nltk
nltk.download('punkt')
from nltk.stem.lancaster import LancasterStemmer
import numpy as np
import tflearn
import random
import pickle

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView


stemmer = LancasterStemmer()
os.environ['TF_CPP_MIN_LOG_LEVEL'] = "2"
ERROR_THRESHOLD = 0.25  # Threshold for filtering out non-confident response.
context = {}


def clean_up_sentence(sentence):
    # tokenize the pattern
    sentence_words = nltk.word_tokenize(sentence)
    # stem each word
    sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
    return sentence_words


# return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
def bow(sentence, words, show_details=False):
    # tokenize the pattern
    sentence_words = clean_up_sentence(sentence)
    # bag of words
    bag = [0]*len(words)
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s:
                bag[i] = 1
                if show_details:
                    print ("found in bag: %s" % w)

    return np.array(bag)


def classify(bot, sentence):
    # generate probabilities from the model
    results = bot['model'].predict([bow(sentence, bot['words'])])[0]
    # filter out predictions below a threshold
    results = [[i, r] for i, r in enumerate(results) if r > ERROR_THRESHOLD]
    # sort by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append((bot['classes'][r[0]], r[1]))
    # return tuple of intent and probability
    return return_list


def response(bot, sentence, user_id='123', show_details=False):
    if not user_id in context:
        context[user_id] = ""
    end_response = None

    results = classify(bot, sentence)
    # if we have a classification then find the matching intent tag
    if results:
        # loop as long as there are matches to process
        while results:
            for i in bot['intents']['intents']:
                # find a tag matching the first result
                if i['tag'] == results[0][0]:

                    # check if this intent is contextual and applies to this user's conversation
                    if not 'context_filter' in i or (user_id in context and 'context_filter' in i and i['context_filter'] == context[user_id]):
                        # if show_details: print ('tag:', i['tag'])
                        # a random response from the intent
                        print('RESPONSE: ' + random.choice(i['responses']))
                        end_response = random.choice(i['responses'])

                        # set context for this intent if necessary
                        if 'context_set' in i:
                            if show_details: print('context:', i['context_set'])
                            context[user_id] = i['context_set']

                            print('CONTEXT: ' + context[user_id])

            results.pop(0)

    if end_response is None:
        for i in bot['errors']['errors']:
            if i['context_filter'] == context[user_id]:
                print('ERROR: ' + random.choice(i['responses']))
                return random.choice(i['responses'])
    return end_response


def load_model(intent):
    # Create Tensorflow graph
    data = pickle.load(open(f'bots/pickles/{intent}.pickle', 'rb'))
    words = data['words']
    classes = data['classes']
    train_x = data['train_x']
    train_y = data['train_y']

    # Import our intents file
    with open(f'bots/intents/{intent}.json') as json_data:
        intents = json.load(json_data)

    # Load saved model
    net = tflearn.input_data(shape=[None, len(train_x[0])])
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
    net = tflearn.regression(net)
    model = tflearn.DNN(net)
    model.load(f'bots/models/{intent}.tflearn')

    # Import our error file
    with open(f'bots/errors/{intent}.json') as json_data:
        errors = json.load(json_data)

    return {
        'model': model,
        'words': words,
        'classes': classes,
        'intents': intents,
        'errors': errors,
    }


bots = {
    'evan': load_model('evan')
}
# print(classify(botEason, 'is your shop open today?'))
# print(response(botEason, 'we want to rent a moped'))


class ChatAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        if kwargs['botName'].lower() == 'evan':
            return Response({
                'username': 'Evan',
                'message': response(bots['evan'], request.data['message'], request.META.get('HTTP_AUTHORIZATION')),
            })
        return Response({'error': f'{kwargs["botName"]} is not supported.'}, status=status.HTTP_400_BAD_REQUEST)
