"""haven URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework.permissions import IsAuthenticated, AllowAny

from django.conf.urls import url, include
from django.views import generic
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)

from django.conf.urls.static import static
from django.conf import settings
from rest_framework import views, serializers, status
from rest_framework.response import Response


class UserSerializer(serializers.Serializer):
    # message = serializers.CharField()
    username = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)


class UserView(views.APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        data = {
            # 'message': request.data['message'],
            'username': request.user.username,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name
        }
        serializer = UserSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', get_schema_view()),
    path('api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/auth/token/obtain/', TokenObtainPairView.as_view()),
    path('api/auth/token/refresh/', TokenRefreshView.as_view()),
    path('api/auth/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('api/auth/user/', UserView.as_view(), name='user'),

    path('api/messenger/', include('dps.messenger.urls'), name='messenger'),

    path('api/haven/', include('dps.haven.urls'), name='haven'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
